package com.selfimpr.captcha.enums;


public enum VerificationCodeType {

    CHAR,   //  字符验证码
    OPERATION,  //  运算验证码
    SLIDE  //  滑动验证码
}
